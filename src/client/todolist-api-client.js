const axios = require('axios')

axios.crossDomain = true

const defaultTimeout = 5000

async function getTodos() {
  const response = await axios
    .get(`${process.env.VUE_APP_TODOLIST_API_BASE_URL}/todos`, defaultTimeout)
    .then(function(response) {
      return returnSuccessResponse(response)
    })
    .catch(function(error) {
      return returnErrorResponse(error)
    })
  return response
}

async function addTodo(description) {
  const request = { description: description }
  const response = await axios
    .post(`${process.env.VUE_APP_TODOLIST_API_BASE_URL}/todos`, request, {
      headers: {
        'Content-Type': 'application/json'
      },
      timeout: defaultTimeout
    })
    .then(function(response) {
      return returnSuccessResponse(response)
    })
    .catch(function(error) {
      return returnErrorResponse(error)
    })
  return response
}

async function removeTodo(id) {
  const response = await axios
    .delete(
      `${process.env.VUE_APP_TODOLIST_API_BASE_URL}/todos/${id}`,
      defaultTimeout
    )
    .then(function(response) {
      return returnSuccessResponse(response)
    })
    .catch(function(error) {
      return returnErrorResponse(error)
    })
  return response
}

async function revertTodoStatus(id) {
  const request = {}
  const response = await axios
    .patch(
      `${process.env.VUE_APP_TODOLIST_API_BASE_URL}/todos/${id}`,
      request,
      defaultTimeout
    )
    .then(function(response) {
      return returnSuccessResponse(response)
    })
    .catch(function(error) {
      return returnErrorResponse(error)
    })
  return response
}

async function removeTodos() {
  const response = await axios
    .delete(
      `${process.env.VUE_APP_TODOLIST_API_BASE_URL}/todos`,
      defaultTimeout
    )
    .then(function(response) {
      return returnSuccessResponse(response)
    })
    .catch(function(error) {
      return returnErrorResponse(error)
    })
  return response
}

function returnSuccessResponse(response) {
  const successResponse = {
    isSuccess: true,
    body: response.data
  }
  return successResponse
}

function returnErrorResponse(error) {
  let errorResponse
  if (error.response !== undefined) {
    errorResponse = {
      isSuccess: false,
      message: error.response.data
    }
  } else {
    errorResponse = {
      isSuccess: false,
      message: error.message
    }
  }
  return errorResponse
}

module.exports = {
  getTodos,
  addTodo,
  removeTodo,
  revertTodoStatus,
  removeTodos
}
