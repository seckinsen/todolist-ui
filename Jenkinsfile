podTemplate(containers: [
    containerTemplate(name: 'node', image: 'node', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'pact', image: 'pactfoundation/pact-mock-service', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'docker', image: 'docker', ttyEnabled: true, command: 'cat'),
    containerTemplate(name: 'k8s-kubectl', image: 'lachlanevenson/k8s-kubectl', ttyEnabled: true, command: 'cat')
        ],
    volumes: [
        hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')
        ]
    ) {
    node(POD_LABEL) {
        stage('fetch project, run unit and cdc tests') {
            git(url: 'https://gitlab.com/seckinsen/todolist-ui.git', branch: 'master', credentialsId: 'seckinsen-gitlab-username_password-credentials')
            container('node') {
                stage('set docker image name') {
                    script {
                        def committer = "git rev-parse HEAD | cut -c 1-8"
                        COMMIT_HASH = sh(returnStdout: true, script: committer).trim()
                        REPOSITORY_NAME = "seckinsen/todolist-ui"
                        IMAGE_NAME = "${REPOSITORY_NAME}:${COMMIT_HASH}"
                        echo "docker image name is ${IMAGE_NAME}"
                    }
                }
                stage('fetch node libraries'){
                    sh 'npm install'
                }
                stage('run unit tests') {
                    script {
                        sh 'npm run test:unit'
                    }
                }
                stage('run cdc:consumer tests') {
                    script {
                        sh 'npm run test:consumer'
                    }
                }
                stage('publish consumer contract') {
                    script {
                        sh "CONSUMER_VERSION=${COMMIT_HASH} npm run test:publish:contract"
                    }
                }
            }
        }
        stage('dockerize project and push image to registry') {
            container('docker') {
                script {
                    sh "docker build -t $IMAGE_NAME ."
                    withCredentials([usernamePassword(credentialsId: 'seckinsen-docker-password', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                        sh 'docker login -u $USERNAME -p $PASSWORD docker.io'
                    }
                    sh "docker push $IMAGE_NAME"
                    sh "docker rmi $IMAGE_NAME"
                }
            }
        }
        stage('deploy project to k8s') {
            container('k8s-kubectl') {
                script {
                    withKubeConfig([credentialsId: 'experimental-cluster-kata-config', serverUrl: 'https://35.246.132.170']) {
                        sh 'apk add gettext'
                        sh 'kubectl apply -f .deploy/service.yml'
                        sh "IMAGE_NAME=$IMAGE_NAME envsubst < .deploy/deployment.yml | kubectl apply -f -"
                    }
                }
            }
        }
    }
}