# Todo list specifications

tags: todolist

The user can be able to add, delete item(s) on todo list and mark todo items as completed or not completed

* Open todo list "http://35.246.212.78:8080/"
 
## Add todo to empty list

* Type "buy some milk" to text box
* Click to add button
* Ensure "buy some milk" todo in list
* Clean todo list

## Add todo to not empty list

* Add "buy some milk" todo in list as "not completed"
* Type "enjoy the assignment" to text box
* Click to add button
* Ensure "enjoy the assignment" todo in list below "buy some milk" todo
* Clean todo list

## Mark not completed todo as completed

* Add "buy some milk" todo in list as "not completed"
* Click on not completed "buy some milk" todo for marking as complete
* Ensure "buy some milk" todo "completed"
* Clean todo list

## Mark completed todo as not completed

* Add "buy some milk" todo in list as "completed"
* Click on completed "buy some milk" todo for marking as not complete
* Ensure "buy some milk" todo "not completed"
* Clean todo list

## Delete todo in list which has only one todo

* Add "rest for a while" todo in list as "not completed"
* Delete "rest for a while" todo in list
* Ensure todo list is empty
* Clean todo list

## Delete todo in list which has more than one todo

* Add "rest for a while" todo in list as "not completed"
* Add "drink water" todo in list as "not completed"
* Delete "rest for a while" todo in list
* Ensure "drink water" todo in list
* Clean todo list