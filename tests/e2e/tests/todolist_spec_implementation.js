/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* globals gauge*/

'use strict'

const {
  openBrowser,
  closeBrowser,
  goto,
  write,
  into,
  textBox,
  waitFor,
  listItem,
  highlight,
  clearHighlights,
  click,
  below,
  evaluate,
  $,
  toRightOf
} = require('taiko')

const assert = require('assert')

const headless = process.env.headless_chrome.toLowerCase() === 'true'

beforeSuite(async () => {
  await openBrowser({
    headless: headless,
    args: [
      '--disable-gpu',
      '--disable-dev-shm-usage',
      '--disable-setuid-sandbox',
      '--no-first-run',
      '--no-sandbox',
      '--no-zygote',
      '--single-process'
    ]
  })
})

afterSuite(async () => {
  await closeBrowser()
})

step('Open todo list <url>', async url => {
  // navigate to url
  await goto(url)
})

step('Type <todo> to text box', async todo => {
  // type item to text box
  await write(todo, into(textBox({ class: 'add-todo-input' })))
  await waitFor(500)
})

step('Click to add button', async () => {
  // click add button
  await click('Add')
  await waitFor(500)
})

step('Ensure <todo> todo in list', async todo => {
  // check item in todo list
  await highlight(listItem({ description: todo }))
  assert.ok(await listItem({ description: todo }).exists())
  await waitFor(500)
  await clearHighlights()
})

step('Clean todo list', async () => {
  await click('Clean List')
  await waitFor(500)
})

step('Add <todo> todo in list as <status>', async (todo, status) => {
  var value = status == 'completed' ? true : false
  await write(todo, into(textBox({ class: 'add-todo-input' })))
  await click('Add')
  if (value) {
    await click(todo)
  }
  await waitFor(1500)
})

step(
  'Ensure <currentTodo> todo in list below <previousTodo> todo',
  async (currentTodo, previousTodo) => {
    // check item in todo list
    await highlight(listItem({ description: currentTodo }))
    assert.ok(
      await listItem({ description: currentTodo }, below(previousTodo)).exists()
    )
    await waitFor(500)
    await clearHighlights()
  }
)

step(
  [
    'Click on not completed <todo> todo for marking as complete',
    'Click on completed <todo> todo for marking as not complete'
  ],
  async todo => {
    await click(todo)
    await waitFor(500)
  }
)

step('Ensure <todo> todo <status>', async (todo, status) => {
  var value = status == 'completed' ? 'true' : 'null'
  assert.ok(
    await evaluate(
      $(`li.todo[description="` + todo + `"] > button.todo > span`),
      element => {
        return element.getAttribute('completed') == value
      }
    )
  )
  await waitFor(500)
})

step('Delete <todo> todo in list', async todo => {
  await click('Delete', toRightOf(todo))
})

step('Ensure todo list is empty', async () => {
  assert.equal(await $('ul.todo-list li').exists(), false)
})
