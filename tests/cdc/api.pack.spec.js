const path = require('path')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const Pact = require('@pact-foundation/pact').Pact
const client = require('../../src/client/todolist-api-client')
const interactions = require('./pact-interactions')
const expect = chai.expect
chai.use(chaiAsPromised)
require('custom-env').env(process.env.NODE_ENV)

const apiPort = process.env.VUE_APP_API_PORT

const provider = new Pact({
  consumer: 'todolist-ui',
  provider: 'todolist-api',
  log: path.resolve(process.cwd(), 'logs', 'pact.log'),
  dir: path.resolve(process.cwd(), 'pacts'),
  logLevel: 'info',
  spec: 2,
  port: parseInt(apiPort),
  pactfileWriteMode: 'overwrite'
})

describe('todo list consumer contruct test', async () => {
  // Setup the provider
  before(() => provider.setup())

  // Write Pact when all tests done
  after(() => provider.finalize())

  // verify with Pact, and reset expectations
  afterEach(() => provider.verify())

  describe('get todos', async () => {
    before(() => {
      provider.addInteraction(interactions.getTodosInteraction)
    })

    it('return get todos response', async () => {
      const response = await client.getTodos()
      expect(response).to.be.a('object')
      expect(response.isSuccess).to.be.true
      expect(response.body).to.be.a('array')
      expect(response.body.length).to.equals(2)
    })
  })

  describe('add valid todo', async () => {
    before(() => {
      provider.addInteraction(interactions.validAddTodoInteraction)
    })

    it('return add todo response', async () => {
      const description = 'enjoy the assignment'
      const response = await client.addTodo(description)
      expect(response).to.be.a('object')
      expect(response.isSuccess).to.be.true
      expect(response.body).to.be.a('object')
      expect(response.body).to.have.property(
        'id',
        'f5e4dd38-ddcf-4ece-8c58-fb5a913b43d9'
      )
    })
  })

  describe('add invalid todo', async () => {
    before(() => {
      provider.addInteraction(interactions.invalidAddTodoInteraction)
    })

    it('return bad request response', async () => {
      const description = ''
      const response = await client.addTodo(description)
      expect(response).to.be.a('object')
      expect(response.isSuccess).to.be.false
    })
  })

  describe('remove exists todo', async () => {
    before(() => {
      provider.addInteraction(interactions.removeExistsTodoInteraction)
    })

    it('return exists todo remove response', async () => {
      const id = 'f5e4dd38-ddcf-4ece-8c58-fb5a913b43d9'
      const response = await client.removeTodo(id)
      expect(response).to.be.a('object')
      expect(response.isSuccess).to.be.true
    })
  })

  describe('remove not exist todo', async () => {
    before(() => {
      provider.addInteraction(interactions.removeNotExistTodoInteraction)
    })

    it('return not exist todo remove response', async () => {
      const id = 'a36a27f0-ed36-41a1-89e4-0bf9dc85126e'
      const response = await client.removeTodo(id)
      expect(response).to.be.a('object')
      expect(response.isSuccess).to.be.false
    })
  })

  describe('revert exists todo status', async () => {
    before(() => {
      provider.addInteraction(interactions.revertExistsTodoStatusInteraction)
    })

    it('return exists todo revert status response', async () => {
      const id = 'f5e4dd38-ddcf-4ece-8c58-fb5a913b43d9'
      const response = await client.revertTodoStatus(id)
      expect(response).to.be.a('object')
      expect(response.isSuccess).to.be.true
    })
  })

  describe('revert not exist todo status', async () => {
    before(() => {
      provider.addInteraction(interactions.revertNotExistTodoStatusInteraction)
    })

    it('return not exist todo revert status response', async () => {
      const id = 'a36a27f0-ed36-41a1-89e4-0bf9dc85126e'
      const response = await client.revertTodoStatus(id)
      expect(response).to.be.a('object')
      expect(response.isSuccess).to.be.false
    })
  })

  describe('remove todos', async () => {
    before(() => {
      provider.addInteraction(interactions.removeTodosInteraction)
    })

    it('return exists todo remove response', async () => {
      const response = await client.removeTodos()
      expect(response).to.be.a('object')
      expect(response.isSuccess).to.be.true
    })
  })
})
