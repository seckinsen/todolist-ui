const { like, uuid } = require('@pact-foundation/pact').Matchers

const validAddTodoRequest = {
  description: like('enjoy the assignment')
}

const invalidAddTodoRequest = {
  description: like('')
}

// TODO try => each_like  [ https://docs.pact.io/getting_started/matching ]
const expectedGetTodosResponse = [
  {
    id: uuid('f5e4dd38-ddcf-4ece-8c58-fb5a913b43d9'),
    description: like('buy some milk'),
    completed: like(false)
  },
  {
    id: uuid('982b62c4-119d-47f1-a249-586004f9d382'),
    description: like('rest for a while'),
    completed: like(false)
  }
]

const expectedAddTodoResponse = {
  id: uuid('f5e4dd38-ddcf-4ece-8c58-fb5a913b43d9')
}

module.exports = {
  getTodosInteraction: {
    state: 'I have todos list',
    uponReceiving: 'a request for getting all todos in the list',
    withRequest: {
      method: 'GET',
      path: '/todos'
    },
    willRespondWith: {
      status: 200,
      headers: {
        'Content-Type': 'application/json'
      },
      body: expectedGetTodosResponse
    }
  },
  validAddTodoInteraction: {
    state: 'I want to add a todo to list',
    uponReceiving: 'a valid request for adding a todo to list',
    withRequest: {
      method: 'POST',
      path: '/todos',
      headers: {
        'Content-Type': 'application/json'
      },
      body: validAddTodoRequest
    },
    willRespondWith: {
      status: 201,
      headers: {
        'Content-Type': 'application/json'
      },
      body: expectedAddTodoResponse
    }
  },
  invalidAddTodoInteraction: {
    state: 'I want to add a todo to list',
    uponReceiving: 'a invalid request for adding a todo to list',
    withRequest: {
      method: 'POST',
      path: '/todos',
      headers: {
        'Content-Type': 'application/json'
      },
      body: invalidAddTodoRequest
    },
    willRespondWith: {
      status: 400
    }
  },
  removeExistsTodoInteraction: {
    state: 'I want to remove exist todo from list',
    uponReceiving: 'a request for removing exist todo from list',
    withRequest: {
      method: 'DELETE',
      path: '/todos/f5e4dd38-ddcf-4ece-8c58-fb5a913b43d9'
    },
    willRespondWith: {
      status: 204
    }
  },
  removeNotExistTodoInteraction: {
    state: 'I want to remove not exist todo from list',
    uponReceiving: 'a request for removing not exist todo from list',
    withRequest: {
      method: 'DELETE',
      path: '/todos/a36a27f0-ed36-41a1-89e4-0bf9dc85126e'
    },
    willRespondWith: {
      status: 404
    }
  },
  revertExistsTodoStatusInteraction: {
    state: 'I want to revert exists todo status',
    uponReceiving: 'a request for revert exists todo status',
    withRequest: {
      method: 'PATCH',
      path: '/todos/f5e4dd38-ddcf-4ece-8c58-fb5a913b43d9'
    },
    willRespondWith: {
      status: 204
    }
  },
  revertNotExistTodoStatusInteraction: {
    state: 'I want to revert not exist todo status',
    uponReceiving: 'a request for revert not exist todo status',
    withRequest: {
      method: 'PATCH',
      path: '/todos/a36a27f0-ed36-41a1-89e4-0bf9dc85126e'
    },
    willRespondWith: {
      status: 404
    }
  },
  removeTodosInteraction: {
    state: 'I want to remove all todos from list',
    uponReceiving: 'a request for removing all todos from list',
    withRequest: {
      method: 'DELETE',
      path: '/todos'
    },
    willRespondWith: {
      status: 204
    }
  }
}
