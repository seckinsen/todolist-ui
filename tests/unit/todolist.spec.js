import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import TodoList from '@/views/TodoList.vue'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
const mock = new MockAdapter(axios)
import flushPromises from 'flush-promises'

const factory = (values = {}) => {
  return shallowMount(TodoList, {
    data() {
      return {
        ...values
      }
    }
  })
}

const baseUrl = process.env.VUE_APP_TODOLIST_API_BASE_URL

describe('TodoList', () => {
  it('should be instantiated', async () => {
    mock.onGet(`${baseUrl}/todos`).reply(200, [])
    const wrapper = factory()
    expect(wrapper.vm.todo).to.equal('')
    expect(wrapper.vm.todos.length).to.equal(0)
  })

  it('should not add todo to list when input text is empty', async () => {
    const description = ''
    mock.onGet(`${baseUrl}/todos`).reply(200, [])
    mock.onPost(`${baseUrl}/todos`, { description: description }).reply(400)
    const wrapper = factory({ todo: '' })
    wrapper.find('.todo-add-button').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.todo).to.equal('')
    expect(wrapper.vm.todos.length).to.equal(0)
    expect(wrapper.find('.todo-list').findAll('li').length).to.equals(0)
  })

  it('should not add todo to list when input text is blank', async () => {
    const description = '        '
    mock.onGet(`${baseUrl}/todos`).reply(200, [])
    mock.onPost(`${baseUrl}/todos`, { description: description }).reply(400)
    const wrapper = factory({ todo: description })
    wrapper.find('.todo-add-button').trigger('click')
    await wrapper.vm.$nextTick()
    await flushPromises()
    expect(wrapper.vm.todo).to.equal('')
    expect(wrapper.vm.todos.length).to.equal(0)
    expect(wrapper.find('.todo-list').findAll('li').length).to.equals(0)
  })

  it('should add todo to list when input text is not blank or empty', async () => {
    const description = 'buy some milk'
    mock.onGet(`${baseUrl}/todos`).reply(200, [])
    mock
      .onPost(`${baseUrl}/todos`, { description: description })
      .reply(201, { id: '519edee8-3038-4a54-85a9-d2874a5a12dc' })
    const wrapper = factory({ todo: description })
    wrapper.find('.todo-add-button').trigger('click')
    await wrapper.vm.$nextTick()
    await flushPromises()
    expect(wrapper.vm.todo).to.equal('')
    expect(wrapper.vm.todos.length).to.equal(1)
    expect(wrapper.find('.todo-list').findAll('li').length).to.equals(1)
  })

  it('should delete todo from list', async () => {
    const todo = {
      id: '519edee8-3038-4a54-85a9-d2874a5a12dc',
      description: 'buy some milk',
      completed: false
    }
    mock.onGet(`${baseUrl}/todos`).reply(200, [todo])
    mock.onDelete(`${baseUrl}/todos/${todo.id}`).reply(204)
    const wrapper = factory()
    await wrapper.vm.$nextTick()
    await flushPromises()
    wrapper.find('.todo-delete-button').trigger('click')
    await wrapper.vm.$nextTick()
    await flushPromises()
    expect(wrapper.vm.todos.length).to.equal(0)
    expect(wrapper.find('.todo-list').findAll('li').length).to.equals(0)
  })

  it('should mark todo as completed', async () => {
    const todo = {
      id: '519edee8-3038-4a54-85a9-d2874a5a12dc',
      description: 'buy some milk',
      completed: false
    }
    mock.onGet(`${baseUrl}/todos`).reply(200, [todo])
    mock.onPatch(`${baseUrl}/todos/${todo.id}`).reply(204)
    const wrapper = factory()
    await wrapper.vm.$nextTick()
    await flushPromises()
    wrapper.find('button.todo').trigger('click')
    await wrapper.vm.$nextTick()
    await flushPromises()
    expect(wrapper.vm.todos[0].completed).to.true
    expect(
      wrapper
        .find('.todo-list')
        .find('li')
        .find('span')
        .attributes('completed')
    ).to.equals('true')
  })

  it('should mark todo as not completed', async () => {
    const todo = {
      id: '519edee8-3038-4a54-85a9-d2874a5a12dc',
      description: 'buy some milk',
      completed: true
    }
    mock.onGet(`${baseUrl}/todos`).reply(200, [todo])
    mock.onPatch(`${baseUrl}/todos/${todo.id}`).reply(204)
    const wrapper = factory()
    await wrapper.vm.$nextTick()
    await flushPromises()
    wrapper.find('button.todo').trigger('click')
    await wrapper.vm.$nextTick()
    await flushPromises()
    expect(wrapper.vm.todos[0].completed).to.false
    expect(
      wrapper
        .find('.todo-list')
        .find('li')
        .find('span')
        .attributes('completed')
    ).to.undefined
  })

  it('should clean todo list', async () => {
    const todos = [
      { description: 'buy some milk', completed: false },
      { description: 'rest for a while', completed: false }
    ]
    mock.onGet(`${baseUrl}/todos`).reply(200, todos)
    mock.onDelete(`${baseUrl}/todos`).reply(204)
    const wrapper = factory()
    await wrapper.vm.$nextTick()
    await flushPromises()
    wrapper.find('button.todo-list-clean-button').trigger('click')
    await wrapper.vm.$nextTick()
    await flushPromises()
    expect(wrapper.vm.todos.length).to.equal(0)
    expect(wrapper.find('.todo-list').findAll('li').length).to.equals(0)
  })
})
