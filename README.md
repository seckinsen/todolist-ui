# TODO List Ui

Deployment Status [![Build Status](http://35.246.133.252:8080/buildStatus/icon?job=todolist-ui-deployment)](http://35.246.133.252:8080/view/todolist/job/todolist-ui-deployment/) | E2E Test Status [![Build Status](http://35.246.133.252:8080/buildStatus/icon?job=todolist-ui-e2e)](http://35.246.133.252:8080/view/todolist/job/todolist-ui-e2e/)

This project aims to build simple todo list page that will meet the following features.

| Environment  | URL                            |
| :----------: | ------------------------------ |
| Production   | http://35.246.212.78:8080/#/   |

### Key Features

- Adding a todo to your list
- Removing a todo from your list
- Marking your not completed todo as completed
- Reverting your todo status from completed to not completed
- Clean up your todo list

Also you can find end to end test scenarios in [here](./tests/e2e/specs/todolist.spec)

#### How To Use

To use the project, you will need [Git](https://git-scm.com) and [npm](https://www.npmjs.com/) installed on your computer.

- Clone this repository.

        git clone https://gitlab.com/seckinsen/todolist-ui.git

- Go into the repository

        cd todolist-ui

- Install dependencies

        npm install

- Execute the following command to compile and hot-reloads the project

        > execute the project on local environment
            npm run serve

        > execute the project on dockerized environment
            ./build.sh

- Then, the application needs to be run on 8080 port.

#### How To Run Unit, CDC Consumer and E2E Tests

- Run unit tests

        npm run test:unit

- Run consumer driven contract tests

        npm run test:consumer

        > Note: execute the following command to publish pact contract

        npm run test:publish:contract

- Run end to end tests

        npm run test:e2e

#### How To Deploy

To deploy the project, you will need [Jenkins](https://www.jenkins.io/) which hosted on [K8S](https://cloud.google.com/solutions/jenkins-on-kubernetes-engine-tutorial). You can find required pipeline file in [here](./Jenkinsfile).

#### How To Run End To End Tests On Pipeline

To run end to end tests periodically, you can define pipeline that runs periodically. You can find required pipeline file in [here](./Jenkinsfile.e2e).

#### In case of any problem

- Do not hesitate to contact with [Seçkin ŞEN](mailto:sckn.sen@gmail.com)
